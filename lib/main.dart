import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_call/screen/hostmeeting.dart';
import 'package:video_call/screen/joinViaCode.dart';
import 'package:video_call/theme.dart';
import 'package:video_call/widgets/callView.dart';
import 'package:video_call/widgets/chatView.dart';
import 'package:video_call/screen/conversation.dart';
import 'package:video_call/screen/dashboard.dart';
import 'package:video_call/screen/forgotPass.dart';
import 'package:video_call/screen/login.dart';
import 'package:video_call/screen/registration.dart';
import 'package:video_call/screen/search.dart';
import 'package:video_call/screen/splash.dart';
import 'package:video_call/screen/videoCall.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

static const platform = const MethodChannel('ourproject.sendstring');
String receivedString ='';

Future<void> callNativeFunction() async {
  String msg = "Hello", data = '';
}

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: AppTheme.primaryColor),
      home: SplashScreen(),
      routes: {
        'splash': (context) => SplashScreen(),
        'login': (context) => LoginScreen(),
        'forgot': (context) => ForgotPassScreen(),
        'dashboard': (context) => Dashboard(),
        'chat_page': (context) => ChatView(),
        'call_page': (context) => CallView(),
        'search': (context) => SearchScreen(),
        'conversation': (context) => ConversationScreen(),
        'video_call': (context) => VideoScreen(),
        'registration': (context) => RegisterScreen(),
        'hostMeeting': (context) => HostMeeting(),
        'joinViaCode': (context) => JoinViaCode(),
      },
    );
  }
}
