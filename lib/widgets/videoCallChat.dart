import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/services/commonServices.dart';
import 'package:video_call/theme.dart';
import 'package:video_call/widgets/textInputDecoration.dart';

class VideoChat1to1 extends StatefulWidget {
  final String roomId;
  final String chatRoomId;
  final bool initiatedFromChat;
  final String secondUser;
  VideoChat1to1({this.roomId, this.chatRoomId, this.initiatedFromChat, this.secondUser});
  @override
  _VideoChat1to1State createState() => _VideoChat1to1State();
}

class _VideoChat1to1State extends State<VideoChat1to1> {

  TextEditingController messageController = TextEditingController();
  Stream <QuerySnapshot> chatMessageStream;
  bool isSendByMe = false;

  Widget chatMessageList() {
    return StreamBuilder(
        stream: chatMessageStream,
        builder: (context, snapshot) {
          return snapshot.hasData ?
          ListView.builder(
              itemCount: snapshot.data.docs.length,
              itemBuilder: (context, index) {
                isSendByMe = DataConstants.userData['username'] ==
                    snapshot.data.docs[index].data()["sendBy"];
                return Container(
                  padding: EdgeInsets.only(
                    left: isSendByMe ? 0 : 18,
                    right: isSendByMe ? 18 : 0,
                  ),
                  margin: EdgeInsets.symmetric(vertical: 5),
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  alignment: isSendByMe ? Alignment.centerRight : Alignment
                      .centerLeft,
                  child: Column(
                    crossAxisAlignment: isSendByMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(
                          color: isSendByMe ? AppTheme.lightPrimaryColor : AppTheme
                              .lightPrimaryColor2,
                          borderRadius: isSendByMe ? BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                              bottomLeft: Radius.circular(20))
                              : BorderRadius.only(topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20)),
                        ),
                        child: Text(checkMessageType(snapshot.data.docs[index]
                            .data()["message"])),
                      ),
                      Container(
                        child: Text(getTimeForDisplay(snapshot.data.docs[index].data()["time"]),
                                style: TextStyle(fontSize: 10),),
                      )
                    ],
                  ),
                );
              }
          ) : Container();
        }
    );
  }

  sendMessage() {
    if (messageController.text.isNotEmpty) {
      Map<String, dynamic> messageMap = {
        "message": {
          "msg": messageController.text,
        },
        "sendBy": DataConstants.userData['username'],
        "time": DateTime
            .now()
            .millisecondsSinceEpoch
      };
      addConversationMessage(widget.roomId, messageMap);
      if(widget.chatRoomId != '') {
        addConversationMessage(widget.chatRoomId, messageMap);
      }
      sendMessageToGroup(widget.chatRoomId, messageMap);
      setState(() {
        messageController.text = '';
      });
    }
}
    @override
    void initState() {
      getConversationMessage(widget.roomId).then((value) {
        setState(() {
          chatMessageStream = value;
        });
      });
      // if(!widget.initiatedFromChat){
      //   List<String> userNameInfo = [DataConstants.userData['username'],widget.secondUser];
      //   Map<String, dynamic> chatRoomMap = {
      //     "users": userNameInfo,
      //     "chatRoomId": widget.roomId
      //   };
        // createChatRoom(widget.roomId, chatRoomMap);

      super.initState();
    }


    @override
    Widget build(BuildContext context) {
      double width = MediaQuery.of(context).size.width;
      return Scaffold(
        appBar: AppBar(
          backgroundColor: AppTheme.primaryColor,
          title: Text(widget.roomId),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back),
          ),
        ),
        body: Stack(
          children: [
            chatMessageList(),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: width,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  children: [
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.symmetric(horizontal: 9, vertical: 8),
                      width: width * 0.8,
                      child: TextFormField(
                        controller: messageController,
                        decoration: inputDecoration(
                          hintText: "Type a message",
                          labelText: "",
                        ),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: sendMessage,
                      child: Icon(Icons.send, color: Colors.white,),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        padding: EdgeInsets.all(20),
                        primary: Colors.blue,
                        onPrimary: Colors.black,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }
}