import 'package:flutter/material.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/screen/conversation.dart';
import 'package:video_call/screen/pickupLayout.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/services/commonServices.dart';
import 'package:video_call/services/sharedPreferences.dart';
import 'package:video_call/theme.dart';

import 'buttonDecoration.dart';

class ChatView extends StatefulWidget {

  @override
  _ChatViewState createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> {

  Stream chatRoomStream;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    getUserChats();
  }

  getUserChats() async {
    setState(() {
      isLoading = true;
    });
    DataConstants.userData = await getPreferences(savedUserInfo);
    getChatRooms(DataConstants.userData['username']).then((snapshots){
      setState(() {
        chatRoomStream = snapshots;
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return PickupLayout(
      scaffold: Scaffold(
        body: isLoading ? Center(child: CircularProgressIndicator()) : SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
               StreamBuilder(stream: chatRoomStream,
                builder: (context, snapshot) {
                  return snapshot.hasData && snapshot.data.docs.length > 0 ?
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data.docs.length,
                    shrinkWrap: true,
                    itemBuilder: (context,  index) {
                      return Column(
                        children: [
                          // Text("has data"),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => ConversationScreen(
                                      chatroomId: snapshot.data.docs[index].data()["chatRoomId"], user: snapshot.data.docs[index].data()["users"],)));
                            },
                            child: Container(
                              height: 50,
                              margin : EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                              padding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: index%2==0 ? AppTheme.primaryColor: AppTheme.lightPrimaryColor,
                              ),

                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(snapshot.data.docs[index]
                                          .data()['chatRoomId']
                                          .toString()
                                          .replaceAll("_", "")
                                          .replaceAll(DataConstants.userData["username"], ""),
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w400,
                                            color: index%2==0 ? AppTheme.textColor: Colors.black,
                                          )),

                                    ],
                                  ),
                                  Icon(Icons.chevron_right_outlined,
                                    color: index%2==0 ? Colors.white: Colors.black,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ) :

                      // Container(
                      //   height: 500,
                      //   color: Colors.red,
                      //   // child: Text("no data"),
                      // );
                  AlertDialog(
                    title: Text("You have no previous chats would you like to talk to me ?"),
                    content: Image.asset('assets/doggo.jpg'),
                    // content: Icon(Icons.animation),
                    actions: <Widget>[
                      GestureDetector(
                        child: buttonDecoration("Chat with me",buttonWidth: width * 0.5),
                        onTap: () {
                          List<String> userNameInfo = [DataConstants.userData['username'], "doggo"];
                          // String chatRoomId = DataConstants.userData['username']  + "_doggo";
                          // Map <String, dynamic> chatRoomMap = {
                          //   "users": userNameInfo,
                          //   "chatRoomId": chatRoomId
                          // };
                          startConversation("doggo", context);
                        },
                        ),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
        floatingActionButton: Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: AppTheme.primaryColor
          ),
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed('search');
            },
            child: Icon(Icons.search, color: Colors.white,),
          ),
        ),
      ),
    );
  }
}
