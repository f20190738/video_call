import 'package:flutter/material.dart';
import 'package:video_call/theme.dart';

Widget buttonDecoration (String dispStr, {double buttonWidth, Color color}) {
  return Container(
    alignment: Alignment.center,
    width: buttonWidth - 40,
    padding: EdgeInsets.symmetric(vertical: 16),
    decoration: BoxDecoration(
      color: color ?? AppTheme.primaryColor,
      borderRadius: BorderRadius.circular(12),
    ),
    child: Text(
        dispStr,
      style: TextStyle(
          color: Colors.white,
          fontSize: 16,
          fontWeight: FontWeight.w600,
    ),
  ),
  );
}

Widget callButtons (Icon icon, {double padding = 12, Color backgroundColor = Colors.white}) {
  return Container(
    padding: EdgeInsets.all(padding),
    margin: EdgeInsets.all(10),
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: backgroundColor,
    ),
    child: icon,
  );
}