import 'package:flutter/material.dart';
import 'package:video_call/widgets/buttonDecoration.dart';
import '../theme.dart';

class CallView extends StatefulWidget {

  @override
  _CallViewState createState() => _CallViewState();
}

class _CallViewState extends State<CallView> {

  List previousCallList = [];

  // joinMeeting(){};

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  margin: EdgeInsets.all(5),
                  child: GestureDetector(
                    onTap:() {
                      Navigator.of(context).pushNamed('joinViaCode');
                  },
                    child: buttonDecoration("Join Via Code", buttonWidth: width*0.5, color: AppTheme.primaryColor),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all( 5),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed('hostMeeting');
                    },
                    child: buttonDecoration("Host a meeting", buttonWidth: width*0.5, color: AppTheme.primaryColor),
                  ),
                )
              ],
            ),
            ListView.builder(
              itemCount: previousCallList.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        // TODO: move to call
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
                        color: AppTheme.lightPrimaryColor,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Name', style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w400
                                )),
                                Text('Call details', style: TextStyle(
                                    fontSize: 12, color: AppTheme.textColor,
                                    fontStyle: FontStyle.italic
                                ))
                              ],
                            ),
                            Icon(Icons.chevron_right_outlined)
                          ],
                        ),
                      ),
                    ),
                    index != previousCallList.length-1 ? Container(height: 1.5, width: width-16, color: AppTheme.greyColor) : Container()
                  ],
                );
              },
            )
          ],
        ),
      ),
      // floatingActionButton: GestureDetector(
      //   onTap: () {
      //   },
      //   child: Icon(Icons.search),
      // ),
    );
  }
}
