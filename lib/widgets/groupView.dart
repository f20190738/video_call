import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/screen/groupChatPage.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/services/sharedPreferences.dart';
import 'package:video_call/theme.dart';
import 'package:video_call/widgets/buttonDecoration.dart';
import 'package:video_call/widgets/textInputDecoration.dart';

class GroupView extends StatefulWidget {

  @override
  _GroupViewState createState() => _GroupViewState();
}

class _GroupViewState extends State<GroupView> {

  Stream groups;
  String userName;
  User firebaseUser;
  String groupName;
  bool isLoading =false;
  String newGroupName;

  @override
  void initState() {
    super.initState();
    getJoinedGroups();
  }

  getJoinedGroups() async {
    setState(() {
      isLoading = true;
    });
    DataConstants.userData = await getPreferences(savedUserInfo);
    getUserGroups(DataConstants.userData['username']).then((snapshots) {
      setState(() {
        groups = snapshots;
        isLoading = false;
      });
    });
  }

  String _destructureId(String res){
    return res.substring(0, res.indexOf('_'));
  }

  String _destructureName(String res){
    return res.substring(res.indexOf('_') + 1);
  }

  void popUpDialog(BuildContext context){
    double width = MediaQuery.of(context).size.width;
    Widget cancelButton = GestureDetector(
      onTap: (){
        Navigator.of(context).pop();
      },
      child: buttonDecoration("Cancel", buttonWidth: width * 0.4),

    );
    Widget createButton = GestureDetector(
      onTap: () async {
        if(newGroupName != null) {
          await
            createGroup(DataConstants.userData['username'], newGroupName);
          Navigator.of(context).pop();
        }
      },
      child: buttonDecoration("Create", buttonWidth: width * 0.4),
    );

    AlertDialog alert = AlertDialog(
      title: Text("Create a group"),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.topLeft,
              child: Text(" Enter Group Name",)
          ),
          SizedBox(height: 8,),
          TextField(
            onChanged: (val) {
              newGroupName = val;
            },
            decoration: inputDecoration(
              hintText: "Enter Group Name",
              labelText: "Group Name",
            ),
          ),
        ],
      ),
      actions: [

        cancelButton,
        createButton
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: isLoading ? Center(child: CircularProgressIndicator(),) : SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            StreamBuilder(stream: groups,
              builder: (context, snapshot) {
              if(snapshot.hasData && snapshot.data.docs.length > 0){
                userName = DataConstants.userData['username'];
                return ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: snapshot.data.docs.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      var gName = snapshot.data.docs[index].data()['groupName'].toString();
                      return Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                print(groupName);
                                Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => GroupChatPage(
                                    userName: userName, groupName: gName,)));
                                },
                              child: Container(
                                height: 50,
                                margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  color: index%2==0 ? AppTheme.primaryColor: AppTheme.lightPrimaryColor,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(gName,
                                          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400,
                                          color: index % 2 == 0 ? AppTheme.textColor : Colors.black),
                                        ),
                                      ],
                                    ),
                                    Icon(Icons.chevron_right_outlined,
                                      color: index % 2 == 0 ? Colors.white : Colors.black,)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    );
                  }
                  return Center(child: Text("No Groups"));
              }
            ),
          ],
        ),
      ),
      floatingActionButton: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: AppTheme.primaryColor
        ),
        child: GestureDetector(
          onTap: () {
            popUpDialog(context);
          },
          child: Icon(Icons.add, color: Colors.white,),
        ),
      ),
    );
  }
}