import 'package:flutter/material.dart';
import 'package:video_call/theme.dart';

InputDecoration inputDecoration({String hintText, String labelText}) {
  return InputDecoration(
    hintText: hintText,
    labelText: labelText,
    labelStyle: TextStyle(color: AppTheme.primaryColor),
    filled: true,
    border: InputBorder.none,
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(8)),
      borderSide: BorderSide(
        color: AppTheme.greyColor,
        width: 2
      )
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(8)),
      borderSide: BorderSide(
        color: AppTheme.primaryColor,
        width: 2
      )
    ),
    contentPadding: EdgeInsets.symmetric(horizontal: 16)
  );
}