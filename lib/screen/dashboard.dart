import 'package:flutter/material.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/services/authentication.dart';
import 'package:video_call/services/sharedPreferences.dart';
import 'package:video_call/theme.dart';
import 'package:video_call/widgets/callView.dart';
import 'package:video_call/widgets/chatView.dart';
import 'package:video_call/widgets/groupView.dart';

class Dashboard extends StatefulWidget {

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with TickerProviderStateMixin{

  Authentication authentication = new Authentication();
  TabController tabController;

  @override
  void initState()  {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
    tabController.addListener(handleTabSelection);
    // setUserData();
  }

  // setUserData() async {
  //   DataConstants.userData = await getPreferences(savedUserInfo);
  //   print(DataConstants.userData);
  // }

  void handleTabSelection() {
    if(tabController.indexIsChanging){
      switch(tabController.index) {
        case 0:
          ChatView();
          break;
        case 1:
          CallView();
          break;
        case 2:
          GroupView();
          break;
      }
    }
  }

  @override

  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Future.delayed(Duration(seconds: 10));
    return DefaultTabController(
      length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppTheme.primaryColor,
            actions: [
              IconButton(icon: Icon(Icons.exit_to_app),
                onPressed: (){
                  setPreferences(isLoggedIn, false);
                  setPreferences(savedUserInfo, {});
                  authentication.signOut();
                  Navigator.of(context).popAndPushNamed('login');
                },
              ),
              DataConstants.userData['username'] == null ? Icon(Icons.format_align_center) : FocusedMenuHolder(
                openWithTap: true,
                onPressed: () {},
                menuItems: [
                  FocusedMenuItem(
                    title: Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("UserName:"),
                          SizedBox(width: 4,),
                          Text(DataConstants.userData['username']),
                        ],
                      )
                    )
                  ),
                  FocusedMenuItem(
                      title: Container(


                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Email:"),
                              SizedBox(width: 4,),
                              Text(DataConstants.userData['email']),
                            ],
                          )
                      )
                  ),
                  FocusedMenuItem(
                      title: Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("age:"),
                              SizedBox(width: 4,),
                              Text(DataConstants.userData['age']),
                            ],
                          )
                      )
                  ),
                  FocusedMenuItem(
                      title: Container(
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Gender:"),
                              SizedBox(width: 4,),
                              Text(DataConstants.userData['gender']),
                            ],
                          )
                      )
                  )
                ],
                child: Container(
                  child: Icon(
                    Icons.format_align_center,
                    color: Colors.white,
                  ),
                ),
              ),

            ],
            title: Text('Video Call App'),
            bottom: TabBar(
              controller: tabController,
              tabs: [
                Icon(Icons.chat_outlined),
                Icon(Icons.group),
                Icon(Icons.video_call_outlined)
              ],
            ),
          ),
          body: TabBarView(
            controller: tabController,
            children: [
              ChatView(),
              GroupView(),
              CallView()
            ],
          ),
        ),
      );
  }
}
