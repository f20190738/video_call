import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/screen/searchUsertoAdd.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/services/callUtils.dart';
import 'package:video_call/services/commonServices.dart';
import 'package:video_call/theme.dart';
import 'package:flutter/material.dart';
import 'package:video_call/widgets/textInputDecoration.dart';

class GroupChatPage extends StatefulWidget {
  final String userName;
  final String groupName;

  GroupChatPage({
    this.userName,
    this.groupName,
  });

  @override
  _GroupChatPageState createState() => _GroupChatPageState();
}
class _GroupChatPageState extends State<GroupChatPage> {
  Stream<QuerySnapshot> groupChatStream;
  TextEditingController groupMessageController = new TextEditingController();

  bool isSendByMe = false;


  Widget chatMessages() {
    return StreamBuilder(
      stream: groupChatStream,
      builder: (context, snapshot) {
        // print(snapshot.data.docs.length);
        return snapshot.hasData && snapshot.data.docs.length > 0 ?
            ListView.builder(
              itemCount: snapshot.data.docs.length,
              itemBuilder: (context, index) {
                isSendByMe = widget.userName == snapshot.data.docs[index].data()['sendBy'];
                return Container(
                  padding: EdgeInsets.only(
                      left: isSendByMe ? 0 : 18,
                      right: isSendByMe ? 18 : 0),
                  margin: EdgeInsets.symmetric(vertical: 5),
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  alignment: isSendByMe ? Alignment.centerRight : Alignment
                      .centerLeft,
                  child: Column(
                    crossAxisAlignment: isSendByMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                    children: [
                      Text(
                        snapshot.data.docs[index].data()['sendBy'].toUpperCase(),
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            letterSpacing: -0.5
                        ),
                      ),
                      SizedBox(height: 2,),
                      Container(
                        margin: isSendByMe ? EdgeInsets.only(left: 30) : EdgeInsets.only(right: 30),
                        padding: EdgeInsets.only(top: 17, bottom: 17, left: 20, right: 50),
                        decoration: BoxDecoration(
                          borderRadius: isSendByMe ? BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20), bottomLeft: Radius.circular(20)) :
                              BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20), bottomRight: Radius.circular(20)),
                          color: isSendByMe ? AppTheme.lightPrimaryColor : AppTheme.lightPrimaryColor2,
                        ),
                        child: Text(
                          checkMessageType(snapshot.data.docs[index].data()["message"]),
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black
                          ),
                        ),
                      ),
                      Container(
                        // alignment: !isSendByMe ? Alignment.topLeft : Alignment.bottomRight,
                        child: Text(getTimeForDisplay(snapshot.data.docs[index].data()["time"]),
                        style: TextStyle(fontSize: 10),),
                      )
                    ],
                  ),
                );
            },
        ): Container();
      },
    );
  }

  sendMessage() {
    if (groupMessageController.text.isNotEmpty) {
      Map<String, dynamic> messageMap = {
        "message": {
          "msg" : groupMessageController.text
        },
        "sendBy": widget.userName,
        "time": DateTime
            .now()
            .millisecondsSinceEpoch,
      };
      sendMessageToGroup(widget.groupName, messageMap);
      setState(() {
        groupMessageController.text = '';
      });
    }
  }

  @override
  void initState() {
    super.initState();
    print(widget.groupName);
    getGroupChats(widget.groupName).then((val) {
      setState(() {
        groupChatStream = val;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppTheme.primaryColor,
        title: Text(widget.groupName),
        actions: [
          IconButton(
            icon: Icon(Icons.video_call,
            color: Colors.white,),
            onPressed: () async {
              CallUtils.dial(
                from: DataConstants.userData['username'],
                to: 'GroupMembers',
                context: context,
                user : widget.userName,
                chatRoomId: widget.groupName,
                secondUser: '',
              );
            }
          ),
          IconButton(
            icon: Icon(Icons.add,
            color: Colors.white,),
            onPressed: () async {
              Navigator.push(context, MaterialPageRoute(builder: (context) => AddUser(groupName: widget.groupName)));
            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          chatMessages(),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Colors.white,
              width: width,
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 9, vertical: 8),
                    width: width - 100,
                    child: TextFormField(
                      controller: groupMessageController,
                      decoration: inputDecoration(
                        hintText: "Type a message",
                        labelText: "",
                      ),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: sendMessage,
                    child: Icon(Icons.send, color: Colors.white,),
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(20),
                      primary: Colors.blue,
                      onPrimary: Colors.black,
                    ),
                  )

                ],
              ),
            ),
          ),

        ],
      ),
    );
  }
}