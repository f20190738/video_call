import 'dart:async';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:video_call/constants/appDetails.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/services/call.dart';
import 'package:video_call/services/callMethods.dart';
import 'package:video_call/services/commonServices.dart';
import 'package:video_call/widgets/buttonDecoration.dart';
import 'package:video_call/widgets/videoCallChat.dart';

class VideoScreen extends StatefulWidget {
  /// non-modifiable channel name of the page
  final String roomName;
  final String chatRoomId;

  // final bool initiatedFromChat;
  /// non-modifiable client role of the page
  final ClientRole role;

  final bool audio;

  final bool video;

  final Call call;

  final bool initiatedFromChat;

  final String secondUser;

  /// Creates a call page with given channel name.
  const VideoScreen(
      { Key key, this.roomName, this.role, this.audio, this.video, this.chatRoomId, this.call, this.initiatedFromChat, this.secondUser })
      : super(key: key);

  @override
  _VideoScreenState createState() => _VideoScreenState();
}

class _VideoScreenState extends State<VideoScreen> {

  bool muted;
  bool muteAll = false;
  bool videoOn;
  final _users = <int>[];
  final _infoStrings = <String>[];
  RtcEngine _engine;
  static final CallMethods callMethods = CallMethods();
  final capForChat = 2;
  final capForMeet = 4;

  @override
  void dispose() {
    // clear users
    _users.clear();
    // destroy sdk
    _engine.leaveChannel();
    _engine.destroy();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    muted = !widget.audio;
    videoOn = widget.video;
    // initialize agora sdk
    initialize();
  }

  Future<void> initialize() async {
    if (APP_ID.isEmpty) {
      setState(() {
        _infoStrings.add(
          'APP_ID missing, please provide your APP_ID in settings.dart',
        );
        _infoStrings.add('Agora Engine is not starting');
      });
      return;
    }
    // if(widget.initiatedFromC

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    // await _engine.enableWebSdkInteroperability(true);
    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    configuration.dimensions = VideoDimensions(1920, 1080);
    await _engine.setVideoEncoderConfiguration(configuration);
    var Token = await getTokenFromRoomName(widget.roomName);
    await _engine.joinChannel(Token, widget.roomName, null, 0);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    _engine = await RtcEngine.create(APP_ID);
    await _engine.enableVideo();
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(widget.role);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    },
        joinChannelSuccess: (channel, uid, elapsed) {
          setState(() {
            final info = 'onJoinChannel: $channel, uid: $uid';
            _infoStrings.add(info);
          });
        },
        leaveChannel: (stats) {
          setState(() {
            _infoStrings.add('onLeaveChannel');
            _users.clear();
          });
        },
        userJoined: (uid, elapsed) {
          setState(() {
            final info = 'userJoined: $uid';
            _infoStrings.add(info);
            _users.add(uid);
          });
        },
        userOffline: (uid, elapsed) {
          setState(() {
            final info = 'userOffline: $uid';
            _infoStrings.add(info);
            _users.remove(uid);
          });
        },
        firstRemoteVideoFrame: (uid, width, height, elapsed) {
          setState(() {
            final info = 'firstRemoteVideo: $uid ${width}x $height';
            _infoStrings.add(info);
          });
        }));
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<StatefulWidget> list = [];
    if (widget.role == ClientRole.Broadcaster) {
      list.add(RtcLocalView.SurfaceView());
    }
    _users.forEach((int uid) => list.add(RtcRemoteView.SurfaceView(uid: uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }


  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    switch (views.length) {
      case 0:
        return Center(
          child: Container(
            child: Text("Please wait for the stream to Start"),
          ),
        );
      case 1:
        return Container(
            child: Column(
              children: <Widget>[_videoView(views[0])],
            ));
      case 2:
        return Container(
            child: Column(
              children: <Widget>[
                _expandedVideoRow([views[0]]),
                _expandedVideoRow([views[1]])
              ],
            ));
      case 3:
        return Container(
            child: Column(
              children: <Widget>[
                _expandedVideoRow(views.sublist(0, 2)),
                _expandedVideoRow(views.sublist(2, 3))
              ],
            ));
      case 4:
        return Container(
            child: Column(
              children: <Widget>[
                _expandedVideoRow(views.sublist(0, 2)),
                _expandedVideoRow(views.sublist(2, 4))
              ],
            ));
      default:
        return Container(
            child: Column(
              // children: <Widget>[_videoView(views[0])],
            ));
    }
  }


  bool checkMemberLimit(int capLimit) {
    return _getRenderViews().length <= capLimit;
  }

  /// Toolbar layout
  Widget _toolbar() {
    if (widget.role == ClientRole.Audience) {
      return Container(
        alignment: Alignment.bottomCenter,
        padding: const EdgeInsets.symmetric(vertical: 24),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RawMaterialButton(
              onPressed: () {
                _onCallEnd(context);
              },
              child: Icon(
                Icons.call_end,
                color: Colors.white,
                size: 20.0,
              ),
              shape: CircleBorder(),
              elevation: 2.0,
              fillColor: Colors.redAccent,
              padding: const EdgeInsets.all(15.0),
            ),

          ],
        ),
      );
    } else {
      return Container(
        alignment: Alignment.bottomCenter,
        padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GestureDetector(
              onTap: () => _onCallEnd(context),
              child: callButtons(Icon(
              Icons.call_end,
              color: Colors.white,
              size: 20.0,
              ), padding: 15, backgroundColor: Colors.redAccent)
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: _onToggleVideo,
                  child: callButtons(Icon(
                    videoOn ? Icons.videocam : Icons.videocam_off,
                    color: videoOn ? Colors.white : Colors.grey,
                    size: 20,), padding: 12, backgroundColor: videoOn ? Colors.blueAccent : Colors.white,)
                ),
                GestureDetector(
                  onTap: _onToggleMute,
                  child: callButtons(Icon(
                    muted ? Icons.mic_off : Icons.mic,
                    color: muted ? Colors.grey : Colors.white,
                    size: 20.0,
                  ), padding: 12, backgroundColor: muted ? Colors.white : Colors.blueAccent,)
                ),
                GestureDetector(
                  onTap: _onSwitchCamera,
                  child: callButtons(Icon(
                    Icons.switch_camera,
                    color: Colors.blueAccent,
                    size: 20.0,
                    ), padding: 12, backgroundColor: Colors.white )
                ),
              ],
            ),
            FocusedMenuHolder(
              openWithTap: true,
              onPressed: () {},
              menuItems: [
                FocusedMenuItem(
                    title: Text("Mute All"),
                    trailingIcon: Icon(Icons.mic_off),
                    onPressed: _muteAll),
                FocusedMenuItem(
                    title: Text("Chat",
                      style: TextStyle(color: checkMemberLimit(capForChat) ? Colors.black : Colors.grey),),
                    trailingIcon: Icon(Icons.message),
                    onPressed:  () {
                      Navigator.push(context,
                        MaterialPageRoute(builder: (context) =>
                            VideoChat1to1(roomId: widget.roomName,
                              chatRoomId: widget.chatRoomId,
                              initiatedFromChat: widget.initiatedFromChat,
                              secondUser: widget.secondUser,),
                        ),
                      );
                    }
                )
              ],
              child: Container(
                width: 44,
                height: 44,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[600],
                      offset: Offset(0.0, 0.1),
                      blurRadius: 6.0,
                    )
                  ],
                  color: Colors.white,
                ),
                child: Icon(
                  Icons.more_vert,
                  color: Colors.blueAccent,
                ),
              ),
            ),

          ],
        ),
      );
    }
  }


  /// Info panel to show logs
  Widget _panel() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 48),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 48),
          child: ListView.builder(
            reverse: true,
            itemCount: _infoStrings.length,
            itemBuilder: (BuildContext context, int index) {
              if (_infoStrings.isEmpty) {
                return Container();
              }
              return Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 3,
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 5,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.yellowAccent,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          _infoStrings[index],
                          style: TextStyle(color: Colors.blueGrey),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _onCallEnd(BuildContext context) {
    Navigator.pop(context);
    callMethods.endCall(call: widget.call);
    // if(widget.initiatedFromChat){
    //   FirebaseFirestore.instance
    // }
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    _engine.muteLocalAudioStream(muted);
  }

  void _onToggleVideo() {
    setState(() {
      videoOn = !videoOn;
    });
    _engine.enableLocalVideo(videoOn);
  }

  void _muteAll() {
    setState(() {
      muteAll = !muteAll;
    });
    _engine.muteAllRemoteAudioStreams(muteAll);
  }

  void _onSwitchCamera() {
    _engine.switchCamera();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Stack(
            children: <Widget>[
              _viewRows(),
              // _panel(),
              _toolbar(),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white12,
                  ),
                  child: Text(widget.roomName, style: TextStyle(color: Colors.white, fontSize: 20, ),),)
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// RawMaterialButton(
//   onPressed: _onToggleVideo,
//   child: Icon(
//     videoOn ? Icons.videocam : Icons.videocam_off,
//     color: videoOn ? Colors.white : Colors.grey,
//     size: 20,
//   ),
//   shape: CircleBorder(),
//   elevation: 2.0,
//   fillColor: videoOn ? Colors.blueAccent : Colors.white,
//   padding: EdgeInsets.all(12.0),
// ),
// RawMaterialButton(
//   onPressed: _onToggleMute,
//   child: Icon(
//     muted ? Icons.mic_off : Icons.mic,
//     color: muted ? Colors.grey : Colors.white,
//     size: 20.0,
//   ),
//   shape: CircleBorder(),
//   elevation: 2.0,
//   fillColor: muted ? Colors.white : Colors.blueAccent,
//   padding: const EdgeInsets.all(12.0),
// ),
// RawMaterialButton(
//   onPressed: _onSwitchCamera,
//   child: Icon(
//     Icons.switch_camera,
//     color: Colors.blueAccent,
//     size: 20.0,
//   ),
//   shape: CircleBorder(),
//   elevation: 2.0,
//   fillColor: Colors.white,
//   padding: const EdgeInsets.all(12.0),
// ),

