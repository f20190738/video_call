import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/screen/conversation.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/services/commonServices.dart';
import 'package:video_call/services/popUp.dart';

import '../theme.dart';

class AddUser extends StatefulWidget {

  final String groupName;
  AddUser({this.groupName});
  @override
  _AddUserState createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {

  TextEditingController _searchController = TextEditingController();
  List memberList = [];
  QuerySnapshot searchSnapShot;

  bool isLoading = false;
  bool haveUserSearched = false;

  @override
  void initState() {
    initiateSearch();
    super.initState();
  }

  initiateSearch() async {
    if(_searchController.text.isNotEmpty)
      setState(() {
        isLoading = true;
      });

    await getUsersbyUserNames(_searchController.text).then((snapshot){
      // if(snapshot != null ) {
      setState(() {
        searchSnapShot = snapshot;
        setState(() {
          isLoading = false;
          haveUserSearched = true;
        });

      });

    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppTheme.primaryColor,
      ),
      body: isLoading ? Center(child: CircularProgressIndicator()) : Container(
        child: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: _searchController,
                        decoration: InputDecoration(
                          hintText: 'Search',
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        initiateSearch();

                      },
                      child: Icon(Icons.search_rounded),
                    )
                  ],
                ),
              ),
              searchSnapShot != null ?
              searchSnapShot.docs.length == 0 ?  Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text( "no results found"),
              ):
              haveUserSearched?
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: searchSnapShot.docs.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    key: Key(index.toString()),
                    onTap: () {
                      addMemberToGroup(widget.groupName, _searchController.text);
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.1,
                      color:  index%2 == 0 ? AppTheme.lightPrimaryColor : AppTheme.lightPrimaryColor2,
                      // color: AppTheme.primaryColor,
                      height: 40,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(searchSnapShot.docs[index]["username"], style: TextStyle(
                                color: Colors.black45,
                                fontSize: 16, fontWeight: FontWeight.w400
                            )),
                            Text(searchSnapShot.docs[index]["email"], style: TextStyle(color: Colors.black,
                                fontSize: 12, fontStyle: FontStyle.italic, fontWeight: FontWeight.w300
                            ))
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ): Container() : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
