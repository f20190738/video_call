import 'package:flutter/material.dart';
import 'package:video_call/services/authentication.dart';
import 'package:video_call/widgets/buttonDecoration.dart';
import 'package:video_call/widgets/textInputDecoration.dart';

class ForgotPassScreen extends StatefulWidget {

  @override
  _ForgotPassScreenState createState() => _ForgotPassScreenState();
}

class _ForgotPassScreenState extends State<ForgotPassScreen> {

  String email;

  final _formKey = GlobalKey<FormState>();
  Authentication authentication = new Authentication();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text('Please enter the registered email id to reset your password'),
            SizedBox(height: 8,),
            Container(
              width: width - 40,
              padding: EdgeInsets.symmetric(horizontal: 6),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                validator: (val) =>
                val.isEmpty ? "Enter email id" : null,
                decoration: inputDecoration(
                  hintText: 'Enter Email',
                  labelText: 'Email',
                ),
                onChanged: (val) {
                  setState(() {
                    email = val;
                  });
                },
              ),
            ),
            SizedBox(height: 8,),
            GestureDetector(
              child: buttonDecoration('Send Reset Password Link', buttonWidth: width-40),
              onTap: () async {
                if(_formKey.currentState.validate()){
                  await authentication.resetPassword(email);
                  Navigator.pop(context);
                }
              },
            ),
            SizedBox(height: 8,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Continue Login? '),
                GestureDetector(
                  child: Text('Login'),
                  onTap: () {
                    Navigator.pop(context);
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
