import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/screen/forgotPass.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/services/authentication.dart';
import 'package:video_call/services/sharedPreferences.dart';
import 'package:video_call/services/validation.dart';
import 'package:video_call/theme.dart';
import 'package:video_call/widgets/buttonDecoration.dart';
import 'package:video_call/widgets/textInputDecoration.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final formKey = GlobalKey<FormState>();
  bool isLoading = false;
  QuerySnapshot querySnapshot;
  Authentication authentication = new Authentication();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();


  @override
  void initState() {
    super.initState();
  }
  


  signIn() async {
    if(formKey.currentState.validate()) {

      setState(() {
        isLoading = true;
      });
      await authentication.signInWIthEmailAndPassword(_emailController.text, _passwordController.text, context).then((value) {
        if(value != null) {
          getUsersbyEmail(_emailController.text).then((val) async {
            print(val); print("val");
            querySnapshot = val;
            print("data fetch");
            if(querySnapshot != null) {
              print("not null");
              var userdata = Map<String, dynamic>.from(
                  querySnapshot.docs[0].data());
              Map data = {
                'email': _emailController.text,
                'username': userdata["username"],
                'age': userdata['age'],
                'gender': userdata['gender']
              };
              setPreferences(savedUserInfo, data);
              setPreferences(isLoggedIn, true);
              setState(() {
                isLoading = false;
              });
              Navigator.of(context).popAndPushNamed('dashboard');
            }
          });

        } else {
          setState(() async {
            isLoading = false;
          });
        }
      });
    }
  }

  bool isKeyboardOn(){
    if(MediaQuery.of(context).viewInsets.bottom == 0)
      return true ;
    else
      return false;
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: isLoading ? Center(child: CircularProgressIndicator( color: AppTheme.primaryColor,)) : Container(
      // body: Container(
        padding: EdgeInsets.symmetric(vertical: 24),
        child: Form(
          key: formKey,
          child: Stack(
            children: [
              Align(
                alignment: Alignment(0.0, -0.7),
                child: Visibility(
                  visible: isKeyboardOn(),
                    child: Icon(Icons.snowboarding, size: 300, color: AppTheme.primaryColor,))
                ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // Container(
                  //     width: width - 40,
                  //     padding: EdgeInsets.symmetric(horizontal: 6),
                  //     child: TextFormField(
                  //         controller: _userNameController,
                  //         decoration: inputDecoration(
                  //           hintText: "Enter UserName",
                  //           labelText: "UserName",
                  //         )
                  //     )
                  // ),
                  SizedBox(
                    height: 12,
                  ),
                  Container(
                    width: width - 40,
                    padding: EdgeInsets.symmetric(horizontal: 6),
                    child: TextFormField(
                      validator: (val){
                        return (
                          validateEmail(val)
                            ? null : "Please Enter Correct Email");
                      },
                      controller: _emailController,
                      decoration: inputDecoration(
                        hintText: "Enter Email",
                        labelText: "Email",
                      )
                    )
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Container(
                    width: width - 40,
                    padding: EdgeInsets.symmetric(horizontal: 6),
                    child: TextFormField(
                      obscureText: true,
                      validator: (val){
                       return val.length > 6
                       ? null : "Password Length should be more than 6 characters";
                      },
                      controller: _passwordController,
                      decoration: inputDecoration(
                        hintText: "Enter Password",
                        labelText: "Password",
                      )
                    ),
                  ),
                  SizedBox(
                    height: 0,
                  ),
                  GestureDetector(
                    child: Container(
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.symmetric(horizontal: 23, vertical: 8),
                      child: Text("Forgot Password?",
                      style: TextStyle(
                        decoration: TextDecoration.underline
                      ),),
                    ),
                    onTap: () {
                          Navigator.of(context).pushNamed('forgot');
                    },
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  GestureDetector(
                    child: buttonDecoration('Sign In', buttonWidth: width, ),
                    onTap: () {
                      signIn();

                    },
                  ),
                  SizedBox(
                    height: 0,
                  ),
                  // Container(
                  //   width: width,
                  //   alignment: Alignment.center,
                  //   padding: EdgeInsets.symmetric(horizontal: 20),
                  //   child: Text("OR"),
                  // ),
                  // SizedBox(
                  //   height: 8,
                  // ),
                  // GestureDetector(
                  //   child: buttonDecoration('Sign In with Google', buttonWidth: width),
                  //   onTap: () async {
                  //     await authentication.signInWithGoogle(context);
                  //   },
                  // ),
                  // SizedBox(
                  //   height: 8,
                  // ),
                  Padding(
                    padding: EdgeInsets.only(right: 23),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          'New User? ',
                        ),
                        GestureDetector(
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            child: Text(
                              'Register Now.',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                              ),
                            )
                          ),
                          onTap: () {
                            Navigator.of(context).pushNamed('registration');
                          },
                        )
                      ],
                    ),
                  )
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}
