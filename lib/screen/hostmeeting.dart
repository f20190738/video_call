import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/screen/videoCall.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/theme.dart';
import 'package:video_call/widgets/buttonDecoration.dart';
import 'package:video_call/widgets/textInputDecoration.dart';


class HostMeeting extends StatefulWidget {
  const HostMeeting({Key key}) : super(key: key);

  @override
  _HostMeetingState createState() => _HostMeetingState();
}

class _HostMeetingState extends State<HostMeeting> {

  bool switchControlAudio = false;
  bool switchControlVideo = false;
  TextEditingController roomNameController = new TextEditingController();

  void joinMeeting() async {
    await _handleCameraAndMic(Permission.camera);
    await _handleCameraAndMic(Permission.microphone);
      List<String> userNameInfo = [DataConstants.userData['username'], ''];
      Map<String, dynamic> chatRoomMap = {
        "users": userNameInfo,
        "chatRoomId": roomNameController.text
      };
      createChatRoom(roomNameController.text, chatRoomMap);
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => VideoScreen(
          roomName: roomNameController.text,
          role: ClientRole.Broadcaster,
          audio: switchControlAudio,
          video: switchControlVideo,
          chatRoomId: '',
          initiatedFromChat: false,
          secondUser: '',
        ),
      ),
    );
  }

  Future<void> _handleCameraAndMic(Permission permission) async {
    final status = await permission.request();
  }

  void toggleSwitchAudio(bool val){
    setState(() {
      switchControlAudio = !switchControlAudio;
    });
  }

  void toggleSwitchVideo(bool val){
    setState(() {
      switchControlVideo = !switchControlVideo;
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppTheme.primaryColor,
      ),
      body: Container(
        padding: EdgeInsets.all(12),
        child: Column(
          children: [
            Container(
              child: TextFormField(
                controller: roomNameController,
                decoration: inputDecoration(
                  hintText: "Enter Room Name",
                  labelText: "Room Name",
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.keyboard_voice),
                Switch(
                  onChanged: toggleSwitchAudio,
                  value: switchControlAudio,
                  activeColor: Colors.blue,
                  activeTrackColor: Colors.cyanAccent,
                  inactiveThumbColor: Colors.white,
                  inactiveTrackColor: Colors.grey,
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.video_call),
                Switch(
                  onChanged: toggleSwitchVideo,
                  value: switchControlVideo,
                  activeColor: Colors.blue,
                  activeTrackColor: Colors.cyanAccent,
                  inactiveThumbColor: Colors.white,
                  inactiveTrackColor: Colors.grey,
                )
              ],
            ),
            GestureDetector(
              onTap: () {
                joinMeeting();
              },
              child: buttonDecoration("Join Meeting", buttonWidth: width * 0.5),
            )
          ],
        ),
      ),
    );
  }
}
