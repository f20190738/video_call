import 'package:flutter/material.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/services/sharedPreferences.dart';

class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  String routeName = 'login';

  getLoginState() async {
    var response = await getPreferences(isLoggedIn);
    if (response != null && response)
      routeName = 'dashboard';
    else routeName = 'login';
    Navigator.of(context).popAndPushNamed(routeName);
  }

  @override
  void initState() {
    getLoginState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      width: width,
      height: height,
      child: Icon(Icons.scatter_plot_rounded,
        size: 100,
        color: Colors.white),
    );
  }
}
