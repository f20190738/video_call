import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/screen/pickupScreen.dart';
import 'package:video_call/services/call.dart';
import 'package:video_call/services/callMethods.dart';
class PickupLayout extends StatelessWidget {

  final Widget scaffold;
  final CallMethods callMethods = CallMethods();

  PickupLayout({ @required this.scaffold});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
      stream: callMethods.callStream(uid: DataConstants.userData['username']),
      builder: (context, snapshot) {
        if(snapshot.hasData && snapshot.data.data() != null) {
          Call call = Call.fromMap(snapshot.data.data());
          return PickupScreen(call: call);
        }
        return scaffold;
      },
    );
  }
}
