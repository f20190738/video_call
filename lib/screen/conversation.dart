import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/services/callUtils.dart';
import 'package:video_call/services/commonServices.dart';
import 'package:video_call/widgets/textInputDecoration.dart';

import '../theme.dart';

class ConversationScreen extends StatefulWidget {
  final String chatroomId;
  final user;
  ConversationScreen({this.chatroomId, this.user});

  @override
  _ConversationScreenState createState() => _ConversationScreenState();
}

class _ConversationScreenState extends State<ConversationScreen> {

  TextEditingController _textController = TextEditingController();
  Stream <QuerySnapshot> chatMessageStream;
  bool isSendByMe = false;

  // RtmCallManager =RtmClient.getRtmCallManager();
  Widget chatMessageList() {
    return StreamBuilder(
        stream: chatMessageStream,
        builder: (context, snapshot) {
          return snapshot.hasData ?
          ListView.builder(
              itemCount: snapshot.data.docs.length,
              itemBuilder: (context, index) {
                isSendByMe = DataConstants.userData['username'] ==
                    snapshot.data.docs[index].data()["sendBy"];
                return Container(
                  padding: EdgeInsets.only(
                      left: isSendByMe ? 0 : 18,
                      right: isSendByMe ? 18 : 0),
                  margin: EdgeInsets.symmetric(vertical: 5),
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  alignment: isSendByMe ? Alignment.centerRight : Alignment
                      .centerLeft,
                  child: Column(
                    crossAxisAlignment: isSendByMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(
                          color: isSendByMe ? AppTheme.lightPrimaryColor : AppTheme
                              .lightPrimaryColor2,
                          borderRadius: isSendByMe ? BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                              bottomLeft: Radius.circular(20))
                              : BorderRadius.only(topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20)),
                        ),
                        child: Text(checkMessageType(snapshot.data.docs[index].data()["message"])),
                      ),
                      Container(
                        child: Text(getTimeForDisplay(snapshot.data.docs[index].data()["time"]),
                                  style: TextStyle(fontSize: 10),),
                      )
                    ],
                  ),
                );
              }
          ) : Container();
        }
    );
  }

  sendMessage() {
    if (_textController.text.isNotEmpty) {
      Map<String, dynamic> messageMap = {
        "message": {
          "msg" :_textController.text,},
        "sendBy": DataConstants.userData['username'],
        "time": DateTime
            .now()
            .millisecondsSinceEpoch
      };
      addConversationMessage(widget.chatroomId, messageMap);
      setState(() {
        _textController.text = "";
      });
    }
  }

  // Future <void> handleCameraAndMic(Permission permission) async {
  //   final status = await permission.request();
  // }

    @override
    void initState() {
      getConversationMessage(widget.chatroomId).then((value) {
        setState(() {
          chatMessageStream = value;
        });
      });
      super.initState();
    }

    String getSecondUser() {
    return widget.user[0];
    }

    @override
    Widget build(BuildContext context) {
      double width = MediaQuery
          .of(context)
          .size
          .width;

      return Scaffold(
        appBar: AppBar(
          backgroundColor: AppTheme.primaryColor,
          title: Text(widget.chatroomId.replaceAll("_", "").replaceAll(DataConstants.userData['username'], "")),
          actions: [
            IconButton(
              icon: Icon(Icons.video_call,
              color: Colors.white,),
              onPressed: () async {
                CallUtils.dial(
                  from: DataConstants.userData['username'],
                  to: widget.user[1],
                  context: context,
                  user :widget.user,
                  chatRoomId: widget.chatroomId,
                  secondUser: widget.user[0],
                );
                // await handleCameraAndMic(Permission.camera);
                // await handleCameraAndMic(Permission.microphone);
                // await Navigator.push(
                //     context,
                // MaterialPageRoute(
                //     builder: (context) => VideoScreen(
                //       roomName: widget.user[0].toString() + "-" + widget.user[1].toString(),
                //       role: ClientRole.Broadcaster,
                //       audio: true,
                //       video: true,
                //       chatRoomId: widget.chatroomId,
                //     ),
                //   // sendNotification();
                // ),
                // );
              },
            )
          ],
        ),
        body: Stack(
          children: [
            chatMessageList(),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                color: Colors.white,
                width: width,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 9, vertical: 8),
                      width: width - 80,
                      child: TextFormField(
                        controller: _textController,
                        decoration: inputDecoration(
                          hintText: "Type a message",
                          labelText: "",
                        ),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: sendMessage,
                        child: Icon(Icons.send, color: Colors.white,size: 18,),
                        style: ElevatedButton.styleFrom(
                          shape: CircleBorder(),
                          padding: EdgeInsets.all(15),
                          primary: AppTheme.primaryColor,
                          onPrimary: Colors.black,
                        ),

                        )

                  ],
                ),
              ),
            )
          ],
        ),
      );
    }
  }


