import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_call/constants/genderdata.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/services/DatabaseAccess.dart';
import 'package:video_call/services/authentication.dart';
import 'package:video_call/services/sharedPreferences.dart';
import 'package:video_call/services/validation.dart';
import 'package:video_call/theme.dart';
import 'package:video_call/widgets/buttonDecoration.dart';
import 'package:video_call/widgets/textInputDecoration.dart';

class RegisterScreen extends StatefulWidget {

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  // bool isLoading =false;
  final formKey = GlobalKey<FormState>();
  int selectedGenderIndex = -1;
  Authentication _authentication = new Authentication();

  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();
  TextEditingController _ageController = new TextEditingController();

  // final formKey = GlobalKey<FormState>();
  signUp(){
    if(formKey.currentState.validate()) {
      Map<String, String> data = {
        'username': _usernameController.text,
        'email': _emailController.text,
        // 'address': _addressController.text,
        'gender': genderDataList[selectedGenderIndex]['value'],
        'age': _ageController.text,
      };

      // setState(() {
      //   isLoading = true;
      // });
      _authentication.signUpWithEmailAndPassword(_emailController.text,_passwordController.text,context).then((value){
            uploadUserInfo(data);
       setPreferences(isLoggedIn, true);
       Navigator.pop(context);
       Navigator.of(context).popAndPushNamed('dashboard');
      });
      setPreferences(savedUserInfo, data);
    }
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    int index = -1;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: AppTheme.primaryColor,
        brightness: Brightness.light,
      ),
      body: Form(
        key: formKey,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextFormField(
                validator: (val){
                  return val.isEmpty || val.length < 3 ? "UserName should have more than three characters" : null;
                },
                decoration: inputDecoration(hintText: 'Enter Name', labelText: 'User Name'),
                controller: _usernameController,
              ),
              SizedBox(height: 12),
              TextFormField(
                validator: (val){
                  return validateEmail(val) ? null : "Please enter correct email";
                },
                controller: _emailController,
                decoration: inputDecoration(
                  hintText: "Enter Email",
                  labelText: "Email",
                ),
              ),
              SizedBox(height: 12),
              TextFormField(
                obscureText: true,
                validator: (val){
                  return val.length>6 ? null : "Password length should be greater than 6 characters";
                },
                controller: _passwordController,
                decoration: inputDecoration(
                  hintText: "Enter Password",
                  labelText: "Password",
                ),
              ),
              SizedBox(height: 12,),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8),
                alignment: Alignment.centerLeft,
                child: Text( "Gender",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: AppTheme.primaryColor
                ),),
              ),
              SizedBox(height: 6,),
              SizedBox(
                height: 50,
                child: Wrap(
                  alignment:WrapAlignment.spaceEvenly,
                  direction: Axis.horizontal,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedGenderIndex = 0;
                        });
                      },
                      child: Container(
                        // margin: EdgeInsets.symmetric(horizontal: selectedGenderIndex == 1 ? 4 : 1),
                        padding: EdgeInsets.symmetric(vertical: 12,horizontal: 16),
                        decoration: BoxDecoration(
                          color: selectedGenderIndex == 0 ? AppTheme.primaryColor : AppTheme.greyColor,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Text(
                          genderDataList[0]['dispStr'],
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedGenderIndex = 1;
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 4),
                        padding: EdgeInsets.symmetric(vertical: 12,horizontal: 16),
                        decoration: BoxDecoration(
                          color: selectedGenderIndex == 1 ? AppTheme.primaryColor : AppTheme.greyColor,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Text(
                          genderDataList[1]['dispStr'],
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedGenderIndex = 2;
                        });
                      },
                      child: Container(
                        // margin: EdgeInsets.symmetric(horizontal: selectedGenderIndex == 1 ? 4 : 1),
                        padding: EdgeInsets.symmetric(vertical: 12,horizontal: 16),
                        decoration: BoxDecoration(
                          color: selectedGenderIndex == 2 ? AppTheme.primaryColor : AppTheme.greyColor,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Text(
                          genderDataList[2]['dispStr'],
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
                // child: ListView.builder(
                //   // padding: EdgeInsets.symmetric(horizontal: 16),
                //   shrinkWrap: true,
                //   scrollDirection: Axis.horizontal,
                //   itemCount: genderDataList.length,
                //   itemBuilder: (BuildContext context, int index){
                //     return GestureDetector(
                //       onTap: () {
                //         setState(() {
                //           selectedGenderIndex = index;
                //         });
                //       },
                //       child: Container(
                //         height: 50,
                //         margin: EdgeInsets.symmetric(horizontal: index == 1 ? 8 : 0),
                //         alignment: Alignment.centerLeft,
                //         padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
                //         decoration: BoxDecoration(
                //             color: selectedGenderIndex == index ? AppTheme.primaryColor : AppTheme.greyColor,
                //           borderRadius: BorderRadius.circular(12),
                //         ),
                //         child: Text(
                //           genderDataList[index]['dispStr'],
                //           style: TextStyle(
                //             color: Colors.white,
                //             fontSize: 16,
                //             fontWeight: FontWeight.w600,
                //           ),
                //         ),
                //       ),
                //
                //       // child: buttonDecoration(genderDataList[index]['dispStr'], buttonWidth: (width - 64)/3,
                //       //     color: selectedGenderIndex == index ? AppTheme.primaryColor : AppTheme.greyColor),
                //     );
                //   },
                // ),
              ),
              SizedBox(height: 12,),
              TextFormField(
                controller: _ageController,
                keyboardType: TextInputType.number,
                decoration: inputDecoration(
                  hintText: "Enter your Age",
                  labelText: "Age",
                ),
              inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
              ],
                validator: (val){
                  return (int.parse(val) > 10 && int.parse(val) < 100) ? null : "Age should be a valid number";
                },
              ),
              // TextFormField(
              //   controller: _addressController,
              //   decoration: inputDecoration(
              //     hintText: "Enter Address",
              //     labelText: "Address",
              //   ),
              // ),
              SizedBox(height: 12,),
              GestureDetector(
                child: buttonDecoration('Sign Up', buttonWidth: width),
               onTap: (){
                  signUp();
                },
              ),
              SizedBox(height: 0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text('Already have an account?  '),
                  GestureDetector(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Text("Sign In.",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                      ),),
                    ),
                    onTap: (){
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      )
    );
  }
}
