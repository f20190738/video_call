import 'package:flutter/material.dart';

class AppTheme{
  static Color primaryColor = Color(0xff253b9d);
  static Color lightPrimaryColor = Color(0xffc3fdb8);
  static Color lightPrimaryColor2 = Color(0xffgit);
  static Color greyColor = Colors.grey;
  static Color textColor = Colors.grey[350];
  static Color borderColor = Colors.grey[400];
}