import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

setPreferences(String key, var data) async {
  var value = json.encode(data);
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setString(key, value);
}

getPreferences(String key) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var response = preferences.getString(key);
  return response == null ? response : json.decode(response);
}
