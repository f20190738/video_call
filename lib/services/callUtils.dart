import 'dart:math';

import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_call/screen/videoCall.dart';
import 'package:video_call/services/callMethods.dart';
import 'package:video_call/services/call.dart';
class CallUtils {

  static final CallMethods callMethods = CallMethods();

  static dial({
    String from,
    String to,
    context,
    user,
    chatRoomId,
    secondUser
  }) async {
    Call call = Call(
      callerName: from,
      receiverName: to,
      channelId: Random().nextInt(1000).toString(),
    );


    Future <void> handleCameraAndMic(Permission permission) async {
      final status = await permission.request();
    }

    bool callMade = await callMethods.makeCall(call);
    print(callMade);
    call.hasDialled = true;

    if(callMade) {
      await handleCameraAndMic(Permission.camera);
      await handleCameraAndMic(Permission.microphone);
      await Navigator.push(
          context,
      MaterialPageRoute(
          builder: (context) => VideoScreen(
            roomName: user[0].toString() + "-" + user[1].toString(),
            role: ClientRole.Broadcaster,
            audio: true,
            video: true,
            chatRoomId: chatRoomId,
            call: call,
            initiatedFromChat: true,
            secondUser: secondUser,
          ),
      ),
      );
    }
  }
}