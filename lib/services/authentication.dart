import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/screen/dashboard.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/services/popUp.dart';
import 'package:video_call/services/sharedPreferences.dart';

class Authentication{
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;


  Future signInWIthEmailAndPassword(String email, String password, context) async {
    try{
      UserCredential userCredential = await _firebaseAuth.signInWithEmailAndPassword
        (email: email, password: password);
      User userdata = userCredential.user;
      print(userdata); print("signin");
      DataConstants.userData = await getPreferences(savedUserInfo);

      return userdata != null ? uid = userdata.uid : null;
    }catch(e) {
      popUp(context, popUpStr: "Invalid email Id or password");
      // if(e.toString() == "[firebase_auth/user-notfound] There is no user record corresponding to this identifier. The user may have been deleted.")
      //   popUp(context, popUpStr: "Invalid emailId");
      return null;
    }
  }

  Future signUpWithEmailAndPassword(String email, String password, context) async {
    try{
      UserCredential userCredential = await _firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);
      User userdata = userCredential.user;
      return userdata != null ? uid = userdata.uid : null;
    }on FirebaseAuthException catch(e) {
      popUp(context, popUpStr: "Account already exists");
      return null;
    }
  }

  Future resetPassword(String email) async {
    try{
      return await _firebaseAuth.sendPasswordResetEmail(email: email);
    }catch(e){
      return null;
    }
  }

  signOut() async {
    try{
      return await _firebaseAuth.signOut();
    } catch (e){
      return null;
    }
  }
}