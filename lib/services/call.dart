class Call {
  String callerName;
  String receiverName;
  String channelId;
  bool hasDialled;

  Call({
    this.callerName,
    this.receiverName,
    this.channelId,
    this.hasDialled, users
});

  Map<String, dynamic> toMap(Call call) {
    Map<String, dynamic> callMap = Map();
    callMap["callerName"] = call.callerName;
    callMap["receiverName"] = call.receiverName;
    callMap["channelId"] = call.channelId;
    callMap["hasDialled"] = call.hasDialled;
    return callMap;
  }

  Call.fromMap(Map callMap){
    this.callerName = callMap["callerName"];
    this.receiverName = callMap["receiverName"];
    this.channelId = callMap["channelId"];
    this.hasDialled = callMap["hasDialled"];
  }
}