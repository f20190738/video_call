import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/services/popUp.dart';

final CollectionReference groupCollection = FirebaseFirestore.instance.collection("groups");
final CollectionReference userCollection = FirebaseFirestore.instance.collection('users');

Future getUsersbyEmail(String email) async {
  print("get user"); print(email);
  var response = await FirebaseFirestore.instance.collection("users").where("email", isEqualTo: email).get();
  print(response);
  print("here");
  return FirebaseFirestore.instance.collection("users").where("email", isEqualTo: email).get().catchError((e) { print(e);});
}

getUsersbyUserName(String userName,context) async {
  return await FirebaseFirestore.instance
      .collection("users")
      .where("username", isEqualTo: userName)
      .get()
      .catchError((e) {
    popUp(context, popUpStr: e.toString());
  });
}

getUsersbyUserNames(String userName) async {
  return await FirebaseFirestore.instance
      .collection("users")
      .where("username", isEqualTo: userName)
      .get()
      .catchError((e) {
  });
}

uploadUserInfo(userMap){
  FirebaseFirestore.instance.collection("users").add(userMap).catchError((e){
  });
}

getChatRooms(String userName) async {
  var result = FirebaseFirestore.instance.collection("chatRoom").where("users",arrayContains: userName);
  print(result);
  return result.snapshots();
}


Future createChatRoom(chatRoomId, chatRoomMap) async {
  FirebaseFirestore.instance.collection("chatRoom").doc(chatRoomId).set(chatRoomMap).catchError((e){
  });
}

addMemberToChatRoom(chatRoomId, String username) async {
  FirebaseFirestore.instance.collection("chatRoom").doc(chatRoomId).update({'users': FieldValue.arrayUnion([username])});
}

getConversationMessage(String chatRoomId) async {
  return FirebaseFirestore.instance.collection("chatRoom").doc(chatRoomId).collection("chats").orderBy("time").snapshots();
}

Future <void> addConversationMessage(String chatRoomId, messageMap) async {
  FirebaseFirestore.instance.collection("chatRoom").doc(chatRoomId).collection("chats").add(messageMap).catchError((e){
  });
}

sendMessageToGroup(String groupName, chatMessageData) {
  // FirebaseFirestore.instance.collection('groups').doc(groupName).collection("messages").add(chatMessageData);
  // FirebaseFirestore.instance.collection('groups').doc(groupName).update({
  //   'recentMessage': chatMessageData['message'],
  //   'recentMessageSender': chatMessageData['sender'],
  //   'recentMessageTime': chatMessageData['time'].toString(),
  // });
  FirebaseFirestore.instance.collection("groups").doc(groupName).collection("chats").add(chatMessageData).catchError((error){
    print(error);
  });
  }

getGroupChats(String groupName) async{
  return FirebaseFirestore.instance.collection('groups').doc(groupName).collection('chats').orderBy('time').snapshots();
}

getUserGroups(String userName) async {
  var result = FirebaseFirestore.instance.collection("groups").where("members", arrayContains: userName);
  return result.snapshots();
}

Future createGroup(String userName, String groupName) async {
  await FirebaseFirestore.instance.collection("groups").doc(groupName).set({"groupName": groupName, "admin": userName, "members": [userName]});
}

addMemberToGroup(groupName, String username) async {
  FirebaseFirestore.instance.collection("groups").doc(groupName).update({'members': FieldValue.arrayUnion([username])});
}