import 'package:flutter/material.dart';

void popUp(BuildContext context, {String popUpStr}) {
  AlertDialog popUp = AlertDialog(
    content: Container(
      width: MediaQuery.of(context).size.width*0.75,
      padding: EdgeInsets.all(16),
      child: Text(popUpStr,
      style: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.w600
      ),),
    ),
  );
  showDialog(context: context, builder: (BuildContext context) {
    return popUp;
  });
}
