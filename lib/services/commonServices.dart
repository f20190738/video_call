import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:video_call/constants/data.dart';
import 'package:video_call/screen/conversation.dart';
import 'package:http/http.dart' as http;
import 'DatabaseAccess.dart';
import 'package:intl/intl.dart';


checkMessageType(message) {
  var returnMsg ='';
  message.runtimeType == String ?
  returnMsg = message:
  message["msg"] != null ?
  returnMsg = message["msg"]
      : returnMsg = "";
  return returnMsg;
}

startConversation(String userName, context){
  List<String> userNameInfo = [DataConstants.userData['username'], userName];
  String chatRoomId = getChatRoomId(DataConstants.userData['username'], userName);
  Map <String, dynamic> chatRoomMap = {
    "users": userNameInfo,
    "chatRoomId": chatRoomId
  };
  createChatRoom(chatRoomId, chatRoomMap);
  Navigator.push(context, MaterialPageRoute(builder: (context) => ConversationScreen(
    chatroomId: chatRoomId, user: userNameInfo,
  )));
}

getChatRoomId(String a, String b) {
  if(a.substring(0,1).codeUnitAt(0) > b.substring(0,1).codeUnitAt(0)){
    return "$b\_$a";
  } else {
    return "$a\_$b";
  }
}

getTokenFromRoomName(String roomName) async {
  String baseUrl = 'https://get-token-from-agora.herokuapp.com/';
  int uid = 0;
  String token;
  var response = await http.get(Uri.parse(baseUrl + 'access_token?channelName=' + roomName));
  if(response != null ) {
    if(response.statusCode == 500){
      return "Channel Name must be specified";
    }
    else{
      Map<String, dynamic> resMap = Map<String, dynamic>.from(json.decode(response.body));
      token = resMap['token'];
    }
  }
  return token;
}

getTimeForDisplay(time){
  var dateTime = DateTime.fromMillisecondsSinceEpoch(time, isUtc: true);
  return DateFormat('kk:mm a, dd MMM yyyy').format(dateTime);
}