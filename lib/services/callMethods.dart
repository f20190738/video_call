import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:video_call/constants/preferencesKey.dart';
import 'package:video_call/services/call.dart';

class CallMethods {
  final CollectionReference callCollection = FirebaseFirestore.instance.collection(callsCollection);

  Stream<DocumentSnapshot> callStream({String uid}) => callCollection.doc(uid).snapshots();

  Future<bool> makeCall(Call call) async {
    try {
      call.hasDialled = true;
      Map<String, dynamic> hasDialledMap = call.toMap(call);
      call.hasDialled = false;
      Map<String, dynamic> hasNotDialledMap = call.toMap(call);

      await callCollection.doc(call.callerName).set(hasDialledMap);
      await callCollection.doc(call.receiverName).set(hasNotDialledMap);
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<bool> endCall({Call call}) async {
    try{
      await callCollection.doc(call.callerName).delete();
      await callCollection.doc(call.receiverName).delete();
      return true;
    } catch(e) {
      return false;
    }
  }
}